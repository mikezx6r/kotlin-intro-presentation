package com.bns.java;

public class WithApply {
    private String property1;
    private String property2;

    public String getProperty1() {
        return property1;
    }

    public void setProperty1(String property1) {
        this.property1 = property1;
    }

    public String getProperty2() {
        return property2;
    }

    public void setProperty2(String property2) {
        this.property2 = property2;
    }

    @Override
    public String toString() {
        return "WithApply{" + "property1='" + property1 + '\'' +
                ", property2='" + property2 + '\'' +
                '}';
    }

    public static void main(String[] args) {
        final WithApply withApply = new WithApply();
        withApply.setProperty1("prop1 apply");
        withApply.setProperty2("prop2 apply");

        final WithApply withApply2 = new WithApply();
        withApply.setProperty1("prop1-2 apply");
        withApply2.setProperty2("prop2-2 apply");

        System.out.println(withApply);
        System.out.println(withApply2);
    }
}
