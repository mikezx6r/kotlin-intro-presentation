package com.bns.java;

import com.bns.kotlin.SwitchKt;

import static java.util.Arrays.asList;

public class Switch {
    public static int showSwitch(String value) {
        int result;

        switch (value) {
            case "something":
                result = 1;
                break;

            default:
                result = 0;
        }
        return result;
    }

    // calling Kotlin from Java
    public static int switchKotlin(String value) {
        return SwitchKt.showSwitch(value);
    }

    public static void main(String[] args) {
        String value = "something";
        System.out.println("Kotlin: " + switchKotlin(value));
        System.out.println("Java: " + showSwitch(value));

        SwitchKt.otherSwitch(28);
        SwitchKt.otherSwitch("some");

        asList(4, 7, 9).forEach(SwitchKt::switchAsCondition);
    }
}
