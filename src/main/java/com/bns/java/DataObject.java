package com.bns.java;

import com.bns.kotlin.DataObjectK;

public class DataObject {
    private String name;
    private final int age;

    public DataObject(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataObject that = (DataObject) o;
        return age == that.age && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        return (31 * result) + age;
    }

    @Override
    public String toString() {
        return "DataObject{" + "name='" + name + '\'' + ", age=" + age + '}';
    }

    public static void main(String[] args) {
        final DataObject dataObject = new DataObject("John Doe", 18);
        dataObject.setName("Some Other Name");
        System.out.println(dataObject);

        final DataObjectK dKotlin = new DataObjectK("John Doe", 18);
        dKotlin.setName("K Name");
        System.out.println(dKotlin);
    }
}
