package com.bns.java;


import com.bns.kotlin.FunctionsUtilKt;
import org.apache.commons.lang3.StringUtils;

public class FunctionsUtil {
    private FunctionsUtil() {
    }

    public static void performSomething(String parm1, String option) {
        if (parm1 == null) {
            throw new IllegalStateException("parm1 requires value");
        }
        if (option == null) {
            option = "default";
        }
        // perform actions
    }

    public static void anotherPerform(String parm1, String option) {
        if (parm1 != null) {
            StringUtils.capitalize(parm1);
        }
    }

    public static void main(String[] args) {
        String test = null;

        performSomething(test, null);

        FunctionsUtilKt.performSomething(test, "option");

    }
}
