package com.bns.kotlin

import com.bns.java.Switch
import java.util.Arrays.asList

fun showSwitch(value: String) = when (value) {
    "something" -> 1
    else -> 0
}

// Calling Java
fun switchJava(value: String) = Switch.showSwitch(value)


fun otherSwitch(x: Any) {
    when (x) {
        is String -> println("$x: X is NaN")
        0, 1 -> println("0 or 1")
        in 5..10 -> println("from 5-10")
        // Wouldn't actually do this as a !, but for demo purposes
        !in 20..30 -> println("not in 20-30")
        else -> println("in 20-30")
    }
}

fun switchAsCondition(x: Int) {
    when {
        x.rem(2) == 0 -> println("x is even")
        x.rem(3) == 0 -> println("x is divisable by 3")
        else -> println("X is something else")
    }
}

fun main() {
    val value = "something"
    println("Kotlin: ${showSwitch(value)}")
    println("Java: ${switchJava(value)}")

    otherSwitch(28)
    otherSwitch("some")

    asList(4, 7, 9).forEach {
        switchAsCondition(it)
    }
}
