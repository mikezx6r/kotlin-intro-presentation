package com.bns.kotlin

fun main(args: Array<String>) {
    val testObject = WithApplyK().apply {
        property1 = "prop1 apply"
        property2 = "prop2 apply"
    }
    println(testObject)

    with(testObject) {
        property1 = "prop1 with"
        property2 = "prop2 with"
    }
    println(testObject)
}

class WithApplyK {
    lateinit var property1: String
    lateinit var property2: String

    override fun toString() = "WithApplyK(property1='$property1', property2='$property2')"

}
