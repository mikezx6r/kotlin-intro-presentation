package com.bns.kotlin

fun performSomething(parm1: String, option: String = "default") {
    // perform actions
}

fun anotherPerform(parm1: String?, option: String = "default") {
    parm1?.capitalize()
}


// Extension functions
fun String.someFunction() = plus("text")

fun main() {
    val x = "somestring"
    println(x)

    println(x.capitalize())

    println(x.someFunction())

//    performSomething(null)
}
