package com.bns.kotlin

import com.bns.java.DataObject

data class DataObjectK(var name: String, val age: Int)

fun main() {
    val dataObject = DataObjectK("John Doe", 18)
    dataObject.name = "Some Other Name"
    println(dataObject)

    val dO2 = DataObjectK(age = 18, name = "Jane")
    println(dO2)

    val dJava = DataObject("Some name", 16)
    println(dJava)
}

