This project contains a presentation to introduce Kotlin, it's features, and why a team would want to adopt it

The code samples are in proper source files.

The generation of the presentation relies on asciidoctor, and it can be output as html, pdf or slideshow.

To build the slideshow, execute `./gradlew ascii`


## TODO

- Add something about "it can cherrypick good ideas from other languages such as Scala and C#, but leave out stuff that is too complex (for now)"